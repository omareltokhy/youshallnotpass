﻿using System;
using System.Linq;

namespace YouShallNotPass
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a new, secure password: ");
            string pw = Convert.ToString(Console.ReadLine());
            Console.WriteLine(SecurePass(pw)); 
        }
        public static string SecurePass(string pass)
        {
            int length = pass.Length;
            int capital = pass.ToCharArray().Where(c => Char.IsUpper(c)).Count();
            int lower = pass.ToCharArray().Where(c => Char.IsLower(c)).Count();
            int digit = pass.ToCharArray().Where(c => Char.IsDigit(c)).Count();
            int spec = pass.ToCharArray().Where(c => Char.IsLetterOrDigit (c)).Count();
            int secure = 0;

            if (length >= 8) secure++;
            if (capital >= 1) secure++;
            if (lower >= 1) secure++;
            if (digit >= 1) secure++;
            if (spec >= 1) secure++;

            if(length < 6)
            {
                return "Invalid PassWord";
            }
            else if(secure == 5)
            {
                return "Strong password!";
            }
            else if(secure < 5 && secure > 2)
            {
                return "Moderate password.";
            }
            else
            {
                return "Weak password.";
            }

        }
    }
}
