using System;
using Xunit;
using YouShallNotPass;


namespace UShallNotPassTests
{
    public class UnitTest1
    {
        [Fact]
        public void SecurePass_TestsIfPassWordIsSecure_ShouldReturnOneToFiveRating()
        {
            //Arrange
            string password = "PassWord7!";
            string expected = "Strong password!";
            //Act
            string actual = Program.SecurePass(password);
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
